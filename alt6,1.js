var game = {
    field: [],
    check: function() {
        if (this.isWinByRows('x') || this.isWinByCols('x')) {
            alert('Выйграли крестики');
        } else if (this.isWinByRows('o') || this.isWinByCols('o')) {
            alert('Выйграли нолики');
        }
    },

    

    isWinByRow: function(row, mark) {
        for (var col = 0; col < this.field.length - 2; col++) {
            if (this.field[row][col] === mark
                && this.field[row][col + 1] === mark
                && this.field[row][col + 2] === mark) {
                return true;
            }
        }
    
        return false;
    },

    isWinByRows: function(mark) {
        for (var row = 0; row < this.field.length; row++) {
            if (this.isWinByRow(row, mark)) {
                return true;
            }
        }
        return false;
    }, 

    isWinByCols: function(mark) {
        for (var col = 0; col < this.field.length; col++) {
            if (this.isWinByCol(col, mark)) {
                return true;
            }
        }
        return false;
    },

    isWinByCol: function(col, mark) {
        for (var row = 0; row < this.field.length - 2; row++) {
            if (this.field[row][col] === mark
                && this.field[row + 1][col] === mark
                && this.field[row + 2][col] === mark) {
                return true;
            }
        }

        return false;
    },

    init: function(fieldSize) {
        

        var totalStep = fieldSize * fieldSize / 2
       
        for (var i = 0; i < fieldSize; i ++) {
             this.field.push([]);
                for (var j = 0; j < fieldSize; j++) {
                     this.field[i][j] = '-';
                 }
             
        }

        var x,o;
        var arr = [];

             for (var k = 0; k < totalStep; k++) {
                x = prompt('Ходит команда крестиков');
                o = prompt('Ходят команда ноликов');
                arr = x.split('.');
                arr[0]--;arr[1]--;
                this.field[arr[0]][arr[1]] = 'x';
                arr = o.split('.');
                arr[0]--;arr[1]--;
                this.field[arr[0]][arr[1]] = 'o';
                console.log(this.field);
                this.check();
             }

             

            
    }
}