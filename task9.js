var game = {
    quantity: null,
    submitStart: null,
    number: 0,
    start: function () {
        this.submitStart =  document.getElementById('start');
        this.quantity =  document.getElementById('quantity');
        this.submitStart.addEventListener('click', this.createSquare.bind(this));
        document.addEventListener('keypress', this.moveKey);
    },

    createSquare: function () {
        var body = document.querySelector('body');

        var blackDiv = document.createElement('div');
        blackDiv.style.width = '10px';
        blackDiv.style.height = '10px';
        blackDiv.style.backgroundColor = 'black';
        blackDiv.id = 'blackDiv';
        blackDiv.style.position = 'absolute';
        body.appendChild(blackDiv);

        for (var i = 0; i < +(this.quantity.value); i++) {
            var div = document.createElement('div');
            div.style.width = '10px';
            div.style.height = '10px';
            div.style.marginTop = '10px';
            div.style.backgroundColor = 'red'
            div.id = i;
            body.appendChild(div);
   
        }
        document.addEventListener('mousemove', this.moveMouse);  
        document.addEventListener('mouseover', this.changeColorAndWin.bind(this))
        

    },

    moveMouse: function (event) {
        var div = document.getElementById('blackDiv');
        div.style.marginLeft = event.clientX - 5 + 'px';
        div.style.marginTop = event.clientY - 25 + 'px';

    },

    moveKey: function (event) {
        var div = document.getElementById('blackDiv');
        var step = 5;
        console.log(event.keyCode);
    },

    changeColorAndWin: function (event) {
        var elem;
        for (var i = 0; i < +(this.quantity.value) ; i++) {
           elem = document.getElementById(i);
           if (elem == event.target && elem.style.backgroundColor === 'red') {
               elem.style.backgroundColor = 'green';
               this.number ++;

           }
           
        }
        if (this.number === parseInt(this.quantity.value)) {
            alert('Конец игры');
            return this.delete();
        }
    },

    delete: function () {
        var elem;
        for (var i = 0; i < +(this.quantity.value) ; i++) {
           elem = document.getElementById(i);
           elem.remove()
           
        }
        this.number = 0;
        document.getElementById('blackDiv').remove();
        this.quantity.value = '';
        this.submitStart.removeEventListener('click', this.createSquare.bind(this));
        document.removeEventListener('mousemove', this.moveMouse); 
        document.removeEventListener('mouseover', this.changeColorAndWin.bind(this))
    }
    
}
document.addEventListener('DOMContentLoaded', game.start.bind(game));