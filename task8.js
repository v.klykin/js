var squares = {


    start : function () {
        var submitColor = document.getElementById('color-submit');
        var body = document.querySelector('body');
        var submit = document.getElementById('add');
        var submitUp = document.getElementById('up');
        var submitDown = document.getElementById('down');
        var submitLeft = document.getElementById('left');
        var submitRight = document.getElementById('right');
        var shift = 20;
        
        var parentDiv = document.createElement('div');
        parentDiv.style.position = 'absolute';
        parentDiv.style.marginTop = '20px';
        parentDiv.style.marginLeft = '0px';
        body.appendChild(parentDiv);

        this.addSquart(parentDiv);

        submit.addEventListener('click',this.addSquart.bind(this, parentDiv));
        submitColor.addEventListener('click',this.changeColor.bind(this, parentDiv));
        submitUp.addEventListener('click',this.moveUp.bind(this, parentDiv, shift));
        submitDown.addEventListener('click',this.moveDown.bind(this, parentDiv, shift));
        submitRight.addEventListener('click',this.moveLeft.bind(this, parentDiv, shift));
        submitLeft.addEventListener('click',this.moveRight.bind(this, parentDiv, shift));
       },

    addSquart: function (parent) {
        var color = document.getElementById('color');
        var div = document.createElement('div');
        var position = 20;
        div.style.width = '100px';
        div.style.height = '100px';
        if(color.value) {
            div.style.backgroundColor = color.value;
        } else {
            div.style.backgroundColor = 'red'
        }
        div.style.marginLeft = position + 'px';
        
        div.style.float = 'left';
        parent.appendChild(div);
    },

    changeColor: function (parent) {
        var color = document.getElementById('color');
        for (var i = 0; i < parent.children.length; i++) {
            parent.children[i].style.backgroundColor = color.value;
        }
    },

    moveUp: function (parent, shift) {
        var position = parseInt(parent.style.marginTop);
        shift = position - shift;     

        parent.style.marginTop = shift + 'px';
    },

    moveDown: function (parent,shift) {
        var position = parseInt(parent.style.marginTop);
        shift = position + shift;     

        parent.style.marginTop = shift + 'px';
    },

    moveLeft: function (parent, shift) {
        var position = parseInt(parent.style.marginLeft);
        shift = position + shift;     

        parent.style.marginLeft = shift + 'px';
    },

    moveRight: function (parent, shift) {
        var position = parseInt(parent.style.marginLeft);
        shift = position - shift;     

        parent.style.marginLeft = shift + 'px';
    }
}