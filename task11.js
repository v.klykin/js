function start () {
    var count = 0;
    var body = document.querySelector('body');
    var tasks = [];
    var headline = document.getElementById('headline');
    var button = document.getElementById('add');
    var content = document.getElementById('content');
    var body = document.querySelector('body');
    button.addEventListener('click', save);
    
    var loadedTasksJson = localStorage.getItem('tasks');
        if (loadedTasksJson !== null) {
            tasks = JSON.parse(loadedTasksJson);
            for (x = 0; x < tasks.length; x++) {
                tasks[x].number = x;
                createDiv(tasks[x]);
                    
            } 
        }
    

    function formatDate() {
        var today = new Date();
        var currDate = today.getDate();
        var currMonth = today.getMonth() + 1;
        var currYear = today.getFullYear();
        return currYear + "-" + currMonth + "-" + currDate;
    
    }
    function save () {
        var task = {
            name: headline.value,
            content: content.value,
            date: formatDate(),
            number: count,
            complected: false
        }
        tasks.push(task);
        count++;
        createDiv(task);

        var tasksJson = JSON.stringify(tasks);
        localStorage.setItem('tasks', tasksJson);
        location.reload()


    }

    function createDiv (task) {
        var div = document.createElement('div');
        div.style.width = '300px';
        div.style.height = '100px';
        div.style.border = '2px solid';
        div.style.marginBottom = '10px';
        body.appendChild(div);

        var headlineName = document.createTextNode('Заголовок: ');
        div.appendChild(headlineName);

       
        var headlineText = document.createTextNode(task.name);
        div.appendChild(headlineText);

        var br = document.createElement('br');
        div.appendChild(br);

        var contentName = document.createTextNode('Описание: ');
        div.appendChild(contentName);

        
        var contentText = document.createTextNode(task.content);
        div.appendChild(contentText);

        headline.value = '';
        content.value = '';

        var br = document.createElement('br');
        div.appendChild(br);

        var dateName = document.createTextNode('Дата: ');
        div.appendChild(dateName);

        var dateText = document.createTextNode(task.date);
        div.appendChild(dateText);

        var br = document.createElement('br');
        div.appendChild(br);
        
        var buttonDel = document.createElement("button");
        buttonDel.innerText = 'Удалить';
        buttonDel.id = task.number;
        div.appendChild(buttonDel);
        
        var buttonEdit = document.createElement('button');
        buttonEdit.innerText = 'Редактировать';
        buttonEdit.id = task.number + 'edit';
        div.appendChild(buttonEdit);

        var checkComplected = document.createElement('input');
        checkComplected.type = 'checkbox';
        if (task.complected === true) {
            checkComplected.checked = 'checked';
        };
        div.appendChild(checkComplected);
        div.appendChild(document.createTextNode('Выполнено'));

        function checkbox (event) { 
            var changeComplected = localStorage.getItem('tasks');
            changeComplected = JSON.parse(changeComplected);         
            if (event.target.checked) {
                task.complected = true;
                changeComplected[task.number].complected = true;    
                } else {
                    task.complected = false;
                    changeComplected[task.number].complected = false;   
                }
            changeComplected = JSON.stringify(changeComplected);    
            localStorage.setItem('tasks', changeComplected);
        }
        checkComplected.addEventListener('change', checkbox)
        buttonDel.addEventListener('click', deletes);

        function deletes () {
            var deleteJson = localStorage.getItem('tasks');
            deleteJson = JSON.parse(deleteJson);
            deleteJson.splice(task.number, 1);
            deleteJson = JSON.stringify(deleteJson);    
            localStorage.setItem('tasks', deleteJson);

            body.removeChild(this.parentNode);
            location.reload()
            
        }

        function edit () {
            var divEdit = this.parentNode;
            var deleteJson = localStorage.getItem('tasks');
            deleteJson = JSON.parse(deleteJson);
            deleteJson.splice(task.number, 1);
            deleteJson = JSON.stringify(deleteJson);    
            localStorage.setItem('tasks', deleteJson);

            body.removeChild(this.parentNode);
            headline.value = divEdit.childNodes[1].data;
            content.value = divEdit.childNodes[4].data;
            tasks.splice(task.number, 1);

        }
        buttonDel.addEventListener('click', deletes);
        buttonEdit.addEventListener('click', edit)
    }
}
document.addEventListener('DOMContentLoaded', start);